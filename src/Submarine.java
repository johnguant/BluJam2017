import Gamepad.JoyStick;
import ecs100.Sound;

import java.io.File;
import java.io.IOException;

public class Submarine implements Tickable {
    float x = 70,
          y = 10;

    float vx = 0,
          vy = 0;

    float ax = 0,
          ay = 0;

    float theta = (float) Math.PI/4;
    float omega = 0;
    float alpha = 0;

    static final float WALL_COLLISION_THICKNESS = 0.1f;

    Sound movingSound, stoppedSound, musicSound, humSound, crashSound;

    JoyStick js;

    SubmarineApplet parentApplet;

    boolean movingSoundIsGoing = false;


    public Submarine(SubmarineApplet parentApplet) {
        this.parentApplet = parentApplet;
        js = SubmarineApplet.js;

        try {
            stoppedSound = new Sound(new File("audio/stopped-loop-1.wav"));
            movingSound = new Sound(new File("audio/moving-loop-1.wav"));
            musicSound = new Sound(new File("audio/music.wav"));
            humSound = new Sound(new File("audio/hum.wav"));
            crashSound = new Sound(new File("audio/crash.wav"));
            musicSound.play();
            humSound.play();

            stoppedSound.setGain(-30f);
            movingSound.setGain(-30f);



        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void stopSounds() {
        stoppedSound.stop();
        movingSound.stop();
        musicSound.stop();
        humSound.stop();

    }

    void bounceNoise() {
        crashSound.stop();
        crashSound.play();
    }

    public void tick(float dt) {


        int xpos = (int)x/10;
        int ypos = (int)y/10;

        if((xpos == 8 || xpos == 9) && (ypos == 10 || ypos == 11) && !SubmarineApplet.gameOver) {
            parentApplet.endGame(true);
            System.out.println("You win!");
        }

        float aFwd = -js.getNormalisedAnalogValue("STICK_Y") / 1;
        float aRight = -js.getNormalisedAnalogValue("STICK_X") / 1;
        alpha = -js.getNormalisedAnalogValue("TWIST") / 10;
        if(SubmarineApplet.gameOver){
            aFwd = 0;
            aRight = 0;
            alpha = 0;
        }

        ay = (aFwd * (float) Math.sin(theta)) + (aRight * (float) Math.cos(theta));
        ax = (aFwd * (float) Math.cos(theta)) - (aRight * (float) Math.sin(theta));

        float accelness = Math.max(Math.max(Math.abs(aFwd), Math.abs(aRight)), Math.abs(alpha*10));
//        movingSound.setGain(((float) Math.max(Math.log(accelness)/Math.log(1.1), -80)));
//        stoppedSound.setGain(((float) Math.max(Math.log(accelness)/Math.log(1.1), -80)));

        if (accelness > 0.2 && !movingSoundIsGoing) {
            System.out.println("MOVE");
            movingSoundIsGoing = true;
            movingSound.play();
            stoppedSound.stop();
        } else if (accelness <= 0.2 && movingSoundIsGoing) {
            System.out.println("STOP MOVE");
            movingSoundIsGoing = false;
            movingSound.stop();
            stoppedSound.play();
        }


        vx += ax*dt;
        vy += ay*dt;
        omega += alpha*dt;

        vx *= Math.pow(0.8, dt);
        vy *= Math.pow(0.8, dt);
        omega *= Math.pow(0.8, dt);

        x += vx*dt;
        y += vy*dt;
        theta += omega*dt;
//        System.out.println(x);
//        System.out.flush();

        float x_matching_maze = x/10f;
        float y_matching_maze = y/10f;

        if (x_matching_maze % 1 < WALL_COLLISION_THICKNESS || x_matching_maze % 1 > 1-WALL_COLLISION_THICKNESS) {
            // We may have hit a vertical wall

            for (MazeWall wall : SubmarineApplet.maze) {
                if (wall.vertical && Math.round(x_matching_maze) == wall.x && Math.floor(y_matching_maze) == wall.y) {
                    if (x_matching_maze < wall.x && vx > 0) {
                        // Bounce left
                        parentApplet.engineeringButtons.killRandomKey();
                        vx = 0.6f*-vx;
                        bounceNoise();
                    } else if (x_matching_maze >= wall.x && vx < 0) {
                        // Bounce right
                        parentApplet.engineeringButtons.killRandomKey();
                        vx = 0.6f*-vx;
                        bounceNoise();
                    } else {
                        // Do nothing, we've already bounced maybe I guess?
                    }
                }
            }
        }

        if (y_matching_maze % 1 < WALL_COLLISION_THICKNESS || y_matching_maze % 1 > 1-WALL_COLLISION_THICKNESS) {
            // We may have hit a horizontal wall

            for (MazeWall wall : SubmarineApplet.maze) {
                if (!wall.vertical && Math.round(y_matching_maze) == wall.y && Math.floor(x_matching_maze) == wall.x) {
                    if (y_matching_maze < wall.y && vy > 0) {
                        // Bounce up??
                        vy = 0.6f*-vy;
                        parentApplet.engineeringButtons.killRandomKey();
                        bounceNoise();
                    } else if (y_matching_maze >= wall.y && vy < 0) {
                        // Bounce down??
                        parentApplet.engineeringButtons.killRandomKey();
                        vy = 0.6f*-vy;
                        bounceNoise();
                    } else {
                        // Do nothing, we've already bounced maybe I guess?
                    }

                }
            }
        }

        //System.out.println(vx + ", " + vy  + ", " + theta);
    }

}
