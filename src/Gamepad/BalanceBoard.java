package Gamepad;

public class BalanceBoard extends Gamepad{
	public BalanceBoard(String filePath) {
		super(filePath);
		setAnalogName("BOTTOM_LEFT", 19, 0, 0);
		setAnalogName("TOP_LEFT", 18, 0, 0);
		setAnalogName("BOTTOM_RIGHT", 17, 0, 0);
		setAnalogName("TOP_RIGHT", 16, 0, 0);
	}
}
