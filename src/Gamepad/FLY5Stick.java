package Gamepad;

public class FLY5Stick extends JoyStick{
	public FLY5Stick(String filePath) {
		super(filePath);
		setButtonName("9", 296);
		setButtonName("10", 297);
		setButtonName("11", 298);
		setButtonName("SWITCH", 301);
		setButtonName("SCROLL_UP", 299);
		setButtonName("SCROLL_DOWN", 300);

		setAnalogName("R_THROTTLE", 2, 0 ,255);
		setAnalogName("STICK_X", 0, 0, 1023);
		setAnalogName("STICK_Y", 1, 0, 1023);
	}
}
