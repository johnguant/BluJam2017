package Gamepad;

public class JoyStick extends Gamepad{

	public JoyStick(String filePath) {
		super(filePath);
		setButtonName("2", 289);
		setButtonName("3", 290);
		setButtonName("4", 291);
		setButtonName("5", 292);
		setButtonName("6", 293);
		setButtonName("7" , 294);
		setButtonName("8", 295);
		setButtonName("TRIGGER", 288);

		setAnalogName("STICK_X", 0, 0, 255);
		setAnalogName("STICK_Y", 1, 0, 255);
		setAnalogName("TWIST", 5, 0 ,255);
		setAnalogName("THROTTLE", 6, 0, 255);
		setAnalogName("DPAD_X", 16, -1, 1);
		setAnalogName("DPAD_Y", 17, -1, 1);
	}
}
