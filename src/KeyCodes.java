import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Stores a map of keycodes to there string values
 */
public class KeyCodes {

	/**
	 * Unmodifiable map of keyCodes
	 */
	public static Map<Integer, String> keyCodes;
	static{
		Map<Integer, String> keyCodes = new HashMap<>();
		keyCodes.put(65, "a");
		keyCodes.put(66, "b");
		keyCodes.put(67, "c");
		keyCodes.put(68, "d");
		keyCodes.put(69, "e");
		keyCodes.put(70, "f");
		keyCodes.put(71, "g");
		keyCodes.put(72, "h");
		keyCodes.put(73, "i");
		keyCodes.put(74, "j");
		keyCodes.put(75, "k");
		keyCodes.put(76, "l");
		keyCodes.put(77, "m");
		keyCodes.put(78, "n");
		keyCodes.put(79, "o");
		keyCodes.put(80, "p");
		keyCodes.put(81, "q");
		keyCodes.put(82, "r");
		keyCodes.put(83, "s");
		keyCodes.put(84, "t");
		keyCodes.put(85, "u");
		keyCodes.put(86, "v");
		keyCodes.put(87, "w");
		keyCodes.put(88, "x");
		keyCodes.put(89, "y");
		keyCodes.put(90, "z");
		keyCodes.put(97, "f1");
		keyCodes.put(98, "f2");
		keyCodes.put(99, "f3");
		keyCodes.put(100, "f4");
		keyCodes.put(101, "f5");
		keyCodes.put(102, "f6");
		keyCodes.put(103, "f7");
		keyCodes.put(104, "f8");
		keyCodes.put(105, "f9");
		keyCodes.put(106, "f10");
		keyCodes.put(107, "f11");
		keyCodes.put(108, "f12");
		keyCodes.put(96, "grave");
		keyCodes.put(49, "1");
		keyCodes.put(50, "2");
		keyCodes.put(51, "3");
		keyCodes.put(52, "4");
		keyCodes.put(53, "5");
		keyCodes.put(54, "6");
		keyCodes.put(55, "7");
		keyCodes.put(56, "8");
		keyCodes.put(57, "9");
		keyCodes.put(48, "0");
		keyCodes.put(45, "minus");
		keyCodes.put(61, "equal");
		keyCodes.put(8, "bspace");
		keyCodes.put(9, "tab");
		keyCodes.put(91, "lbrace");
		keyCodes.put(93, "rbrace");
		keyCodes.put(92, "bslash");
		keyCodes.put(20, "caps");
		keyCodes.put(59, "colon");
		keyCodes.put(10, "enter");
		keyCodes.put(44, "comma");
		keyCodes.put(46, "dot");
		keyCodes.put(47, "slash");
		keyCodes.put(18, "lalt");
		keyCodes.put(32, "space");
		keyCodes.put(19, "ralt");
		keyCodes.put(157, "rwin");
		keyCodes.put(153, "rmenu");
		keyCodes.put(23, "scroll");
		keyCodes.put(22, "pause");
		keyCodes.put(26, "ins");
		keyCodes.put(2, "home");
		keyCodes.put(147, "del");
		keyCodes.put(3, "end");
		keyCodes.put(11, "pgdn");
		keyCodes.put(38, "up");
		keyCodes.put(37, "left");
		keyCodes.put(40, "down");
		keyCodes.put(39, "right");

		// Set map to unmodifiable to people can't change it
		KeyCodes.keyCodes = Collections.unmodifiableMap(keyCodes);
	}
}
