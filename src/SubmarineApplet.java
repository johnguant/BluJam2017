import Gamepad.JoyStick;
import processing.core.PApplet;
import processing.core.PImage;
import processing.opengl.PShader;

import java.nio.channels.Pipe;
import java.nio.file.FileSystems;
import java.util.List;
import java.util.Random;

public class SubmarineApplet extends PApplet {

    static final float FADE_BLACK_TIME = 1;

    static float fadeProgress = FADE_BLACK_TIME;
    static boolean fading = false;

    static Submarine sub;
    EngineeringButtons engineeringButtons;

    static JoyStick js;

    static int millisLastFrame = 0;
    static int millis = 0;

    static List<MazeWall> maze = MazeParser.parse(FileSystems.getDefault().getPath("maze"));

    static PImage pilotHud;
    static PImage rockWall2;
    static PImage winImage;
    static PImage loseImage;
    static PImage endGameImage;

    PImage[] horizWalls;
    PImage[] vertWalls;
    PImage[] ceilImages;

    boolean restart = false;

    public static boolean gameOver = false;

    static SubmarineApplet applet;

    PShader fog;

    public static void main(String[] args) {
        js = (JoyStick) JoyStick.createGamepad(args[0], Integer.valueOf(args[1]));
        PApplet.runSketch(new String[]{"SubmarineApplet"}, new SubmarineApplet());
        PApplet.runSketch(new String[]{"EngineerApplet"} , new EngineerApplet());
    }

    public void settings() {
        size(1920, 1080, P3D);
        fullScreen(1);
        pilotHud = loadImage("pilotScreen.png");
        rockWall2 = loadImage("wall2scaled.png");
        winImage = loadImage("winScreen.png");
        loseImage = loadImage("lossScreen.png");
        endGameImage = winImage;
        ceilImages = new PImage[]{loadImage("roof1.png"), loadImage("roof2.png"), loadImage("roof3.png"), loadImage("roof4.png")};
    }

    public void setup() {
    	restart = false;
    	gameOver = false;
        js.init();
        if (sub != null)
            sub.stopSounds();
        sub = new Submarine(this);
        engineeringButtons = new EngineeringButtons(this);
        fog = loadShader("fogfrag.glsl", "fogvert.glsl");
        applet = this;
        millis = 0;
        millisLastFrame = 0;
        fadeProgress = FADE_BLACK_TIME;
        horizWalls = new PImage[]{loadImage("wall1horiz.png"), loadImage("wall2horiz.png"), loadImage("wall3horiz.png"), loadImage("wall4horiz.png")};
        vertWalls = new PImage[]{loadImage("wall1vert.png"), loadImage("wall2vert.png"), loadImage("wall3vert.png"), loadImage("wall4vert.png")};
        for(MazeWall wall : maze){
            if(wall.vertical){
                wall.wallImage = vertWalls[(int)(Math.random()*vertWalls.length)];
            } else {
                wall.wallImage = horizWalls[(int)(Math.random()*horizWalls.length)];
            }
        }
    }

    public void draw() {
    	shader(fog);
        background(color(0, 0, 127));
        millisLastFrame = millis;
        millis = millis();

        float dt = (millis - millisLastFrame) / 1000f;
        if (dt < 1) {
            sub.tick(dt);
            engineeringButtons.tick(dt);
        }

        if(gameOver && fading){
            fadeProgress -= dt;
            if(fadeProgress < 0) {
                fading = false;
            }
        }

        camera(sub.x, sub.y, 0, (float) Math.cos(sub.theta) + sub.x, (float) Math.sin(sub.theta) + sub.y,0, 0, 0, 1.0f);
        float fov = PI/3;
        float cameraZ = (float)( (height/2.0) / tan((float)(fov/2.0)));
        perspective(fov, (float)width/(float)height, (float)(cameraZ/100000.0),(float) (cameraZ*10.0));


        for (MazeWall wall : maze) {
            pushMatrix();
            translate((float) wall.x*10, (float) wall.y*10, -5);

            PImage wallImage;
            if (wall.vertical) {
                fill(255,0,0);
                rotateY(-TAU/4);
            } else {
                fill(0,255,0);
                rotateX(TAU/4);
            }
            image(wall.wallImage, 0, 0, 10, 10);
            //rect(0,0,10,10);
            popMatrix();
        }

        Random random = new Random(845786543);
        for(int x = 0; x < 12; x++){
            for(int y = 0; y < 13; y++){
                pushMatrix();
                translate((float) x*10, (float)y*10, 5);
                image(rockWall2, 0, 0, 10, 10);
                popMatrix();
                if((x == 8 || x == 9) && (y == 10 || y == 11))
                    continue;
                pushMatrix();
                translate((float) x*10, (float)y*10, -5);
                image(ceilImages[random.nextInt(ceilImages.length)], 0, 0, 10, 10);
                popMatrix();
            }
        }

        resetShader();
        camera();
        hint(DISABLE_DEPTH_TEST);
        noTint();
        image(pilotHud, 0, 0);
        fill(color(0));
        stroke(color(0));
        int y = (int) (615 + (280 - ((280)*((double)engineeringButtons.totalDeadKeys/engineeringButtons.keys.length))));
        rect(1765, y, 50, 5);//895 615 //0 keys.length totaldeadkeys
        //895 - 615 = 280
        int fadeValue =  255 - Math.max((int) ((255)*((fadeProgress)/(FADE_BLACK_TIME))), 0);
        tint(255, fadeValue);
        image(endGameImage, 0, 0, 1920, 1080);
        hint(ENABLE_DEPTH_TEST);

        if(restart){
            restart();
        }
    }

    private void restart(){
        restart = false;
        gameOver = false;
        sub.stopSounds();
        engineeringButtons = new EngineeringButtons(this);
        sub = new Submarine(this);
        fading = false;
        fadeProgress = FADE_BLACK_TIME;

    }

    void endGame(boolean win){
        gameOver = true;
        fadeProgress = FADE_BLACK_TIME;
        fading = true;
        if(win){
            endGameImage = winImage;
        } else {
            endGameImage = loseImage;
        }
        engineeringButtons = new EngineeringButtons(this);
    }

    public void keyPressed(){
    	keyPressed(keyCode);
    }

    public void keyPressed(int keyCode){
        if(engineeringButtons != null)
            engineeringButtons.keyPressed(keyCode);
        if(gameOver && KeyCodes.keyCodes.get(keyCode).equals("r"))
            restart = true;
    }

}
