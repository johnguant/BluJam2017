import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;
import processing.opengl.PGraphics2D;
import processing.opengl.PShader;

/**
 * The engineers screen in the submarine
 *
 * Only handles drawing all gameplay is handled in the SubmarineApplet
 */
public class EngineerApplet extends PApplet{

    static PImage radarMask;
    static PImage radarMapMask;
    static PImage radarGradient;
	static PImage xMark;

    static PGraphics radar;
    static PGraphics radarMap;
    static PGraphics radarMapMaskRotated;

    PShader radarFixupShader;

    static int MAZE_WIDTH = 11;
    static int MAZE_HEIGHT = 12;

    static int MAZE_SQUARE_SIZE = 8;

    static int RADAR_RADIUS = 75;
    static int RADAR_DIAMETER = RADAR_RADIUS*2;
    private PGraphics maze;

    int millisLastFrame = 0;
    int millis = 0;

    float theta = 0;

    Float oldSubX;
    Float oldSubY;

	static PImage engineerScreen;
	Light[] lights;

	public void settings() {
		size(1920, 1080, P2D);
		fullScreen(2);
		engineerScreen = loadImage("navigatorScreen2.png");
		radarMask = loadImage("radar-mask.png");
		radarMapMask = loadImage("radar-map-mask.png");
		radarGradient = loadImage("radar-gradient.png");
		xMark = loadImage("x-mark.png");
		lights = new Light[]{new Light("light1.png", 986, 126),
			 	 new Light("light2.png", 1608, 133),
			 	 new Light("light3.png", 1633, 828),
				 new Light("light4.png", 1390, 897),
				 new Light("light5.png", 937, 832),
				 new Light("light6.png", 882, 872)};
	}

    public void setup() {
		((PGraphics2D)g).textureSampling(2);

		updateMaze();
		radar = createGraphics(RADAR_DIAMETER, RADAR_DIAMETER);
		radarMap = createGraphics(RADAR_DIAMETER, RADAR_DIAMETER, P2D);
		radarMapMaskRotated = createGraphics(RADAR_DIAMETER, RADAR_DIAMETER);

		radarFixupShader = loadShader("data/radar.glsl");

    }

	public void updateMaze(){
		maze = createGraphics(150, 150);
		maze.beginDraw();
		maze.clear();
		maze.stroke(21,254,0);
		maze.fill(21,254,0);
		maze.pushMatrix();
		maze.translate((RADAR_DIAMETER - MAZE_WIDTH*MAZE_SQUARE_SIZE) / 2, (RADAR_DIAMETER - MAZE_HEIGHT*MAZE_SQUARE_SIZE) / 2);

		for(MazeWall wall : SubmarineApplet.maze){
			float x = (float) ((wall.x*MAZE_SQUARE_SIZE)-5);
			float y = (float) (MAZE_HEIGHT*MAZE_SQUARE_SIZE-(wall.y*MAZE_SQUARE_SIZE)+2.5);
			float x2 = x;
			float y2 = y;
			if(wall.vertical)
				y2 -= MAZE_SQUARE_SIZE;
			else {
				x2 += MAZE_SQUARE_SIZE;
			}
			maze.line(x,y,x2,y2);
		}
		if(SubmarineApplet.sub != null) {
			//maze.rect(((oldSubX / 10) * MAZE_SQUARE_SIZE) - 5 - 0.5f, (MAZE_SQUARE_SIZE * MAZE_HEIGHT) - (((oldSubY / 10) * MAZE_SQUARE_SIZE) + 2.5f - 3.5f), 2, 2);
			maze.rect(((SubmarineApplet.sub.x / 10) * MAZE_SQUARE_SIZE) - 5 - 0.5f, (MAZE_SQUARE_SIZE * MAZE_HEIGHT) - (((SubmarineApplet.sub.y / 10) * MAZE_SQUARE_SIZE) + 2.5f - 3.5f), 2, 2);
		}
		maze.popMatrix();
		maze.endDraw();
	}

	public void draw(){
		noTint();
		millisLastFrame = millis;
		millis = millis();

		float dt = (millis - millisLastFrame) / 1000f;
		background(255);
		image(engineerScreen, 0, 0);
		for(Light light : lights){
			light.tick(dt);
			if(light.on)
				image(light.image, light.x, light.y);
		}
		int c = color(0, 0, 0);
		stroke(c);
		color(0);

		theta += dt * 2;


        if (SubmarineApplet.sub != null) {
			if(oldSubX == null || oldSubY == null){
				oldSubX = SubmarineApplet.sub.x;
				oldSubY = SubmarineApplet.sub.y;
			}
			float sub_theta = (float) ((TAU / 4) - (Math.atan2(SubmarineApplet.sub.y - (MAZE_WIDTH * 5) - 5, SubmarineApplet.sub.x - (MAZE_HEIGHT * 5))));
			if (Math.abs(((theta % TAU) - (sub_theta % TAU))%TAU) < 0.2) {
				updateMaze();
			}
			/*float old_sub_theta = (float) ((TAU / 4) - (Math.atan2(oldSubY - (MAZE_WIDTH * 5) - 5, oldSubX - (MAZE_HEIGHT * 5))));
			if (Math.abs((theta % TAU) - (sub_theta % TAU)) < 0.2) {
				oldSubY = SubmarineApplet.sub.y;
				oldSubX = SubmarineApplet.sub.x;
				updateMaze();
			}*/
		}


		radar.beginDraw();
		radar.imageMode(CENTER);
		radar.pushMatrix();
		radar.translate(RADAR_RADIUS, RADAR_RADIUS);
		radar.rotate(theta);
		radar.image(radarGradient, 0,0);
		radar.popMatrix();
		radar.mask(radarMask);
		radar.endDraw();


		radarMapMaskRotated.beginDraw();
		radarMapMaskRotated.clear();
		radarMapMaskRotated.imageMode(CENTER);
		radarMapMaskRotated.pushMatrix();
		radarMapMaskRotated.translate(RADAR_RADIUS, RADAR_RADIUS);
		radarMapMaskRotated.rotate(PI-theta);
		radarMapMaskRotated.image(radarMapMask, 0,0);
		radarMapMaskRotated.popMatrix();
		radarMapMaskRotated.endDraw();

		radarMap.beginDraw();
		radarMap.clear();
		radarMap.imageMode(CENTER);
		radarMap.image(maze, RADAR_RADIUS, RADAR_RADIUS);
		radarMap.mask(radarMapMaskRotated);
		radarMap.filter(radarFixupShader);
		radarMap.endDraw();

		image(radar, 273*5 - RADAR_RADIUS*5, 108*5 - RADAR_RADIUS*5, RADAR_DIAMETER*5, RADAR_DIAMETER*5);
		image(radarMap, 273*5 - RADAR_RADIUS*5, 108*5 - RADAR_RADIUS*5, RADAR_DIAMETER*5, RADAR_DIAMETER*5);
		image(xMark, 1440, 320);


		int fadeValue =  255 - Math.max((int) ((255)*((SubmarineApplet.fadeProgress)/(SubmarineApplet.FADE_BLACK_TIME))), 0);
		tint(255, fadeValue);
		image(SubmarineApplet.endGameImage, 0, 0, 1920, 1080);


//		PGraphics radar = createGraphics(RADAR_DIAMETER, RADAR_DIAMETER);
//
//		radar.beginDraw();
//		radar.translate(RADAR_RADIUS, RADAR_RADIUS);
//		for (float x = -RADAR_DIAMETER; x < RADAR_DIAMETER; x++) {
//			for (float y = -RADAR_DIAMETER; y < RADAR_DIAMETER; y++) {
//				float pixelTheta = atan2(y, x);
//
//				radar.stroke(radarColor((theta + pixelTheta) % TAU));
//				radar.point(x,y);
//
//			}
//		}
//		radar.endDraw();
	}
	public void keyPressed(){
		if (SubmarineApplet.applet != null)
            SubmarineApplet.applet.keyPressed(keyCode);
	}

	/**
	 * Stores state of lights on the dashboard and changes them from off to on
	 */
	private class Light{
		/**
		 * Maximum amount of time a light can stay off
		 */
		static final double MAX_TIMER = 5;
		/**
		 * Minimum amount of time a light can stay off
		 */
		static final double MIN_TIMER = 1;
		/**
		 * How long a light will stay on
		 */
		static final double TIME_ON = 0.25;

		/**
		 * Image file for a lights glow overlay
		 */
		PImage image;

		/**
		 * leftmost x position
		 */
		int x;
		/**
		 * topmost y position
		 */
		int y;

		/**
		 * How long tell the light next switches
		 */
		double timer = 0;

		/**
		 * Whether the light is currently turned on
		 */
		boolean on = false;
		Light(String fPath, int x, int y){
			this.x = x;
			this.y = y;
			image = loadImage(fPath);
			timer = MIN_TIMER + (Math.random()*(MAX_TIMER-MIN_TIMER));
		}

		/**
		 * Each tick checks if the state of the light should be switched
		 *
		 * @param dt time in seconds since method was last called
		 */
		public void tick(float dt){
			timer -= dt;
			// If time expired switch light and reset timer.
			if(timer <= 0) {
				on = !on;
				if(on)
					timer = TIME_ON;
				else
					timer = MIN_TIMER + (Math.random()*(MAX_TIMER-MIN_TIMER));
			}
		}
	}
}
