import java.io.*;
import java.util.*;

/**
 * Used to control keyboard presses and the LEDs on the keyboard
 */
public class EngineeringButtons implements Tickable{

	/**
	 * How long a LED blink is. x time on x time off
	 */
	private final float FLASH_TIME = 0.1f;

	/**
	 * How long in seconds it takes a key to fade from green
	 */
	private final float GREEN_FADE_TIME = 1;

	/**
	 * Minimum amount of time a key can be red
	 */
	private final double MIN_RED_TIME = 0.5;

	/**
	 * Exponential rate at which the time to press a red key shrinks
	 */
	private final double RATE_RED_FASTER = 0.98;

	/**
	 * Exponential rate that secsToKeyOff changes at
	 */
	private final double RATE_SECS_TO_KEY = 0.99;

	/**
	 * How long between new keys lighting up red
	 */
	private double secsToKeyOff = 1;

	/**
	 * Percentage above and below the secsToKeyOff value that the time might scale to
	 */
	private double secsToKeyPercent = 0.1;

	/**
	 * The exact time tel the next key lights up red randomly generated from secsToKeyOff and secsToKeyPercent
	 */
	private double secsToKey;

	/**
	 * How long it has been since the last red key lit up
	 */
	private double currentTime = 0;

	/**
	 * How long a key will remain red before becoming blue
	 */
	private double timeRed = 8;

	/**
	 * How long left as red a key should have when it starts flashing
	 */
	private double timeRedFlash = 4;

	/**
	 * Writer for writing to the keyboards named pipe
	 *
	 * WARNING: Will be null on computers without a corsair keyboard be careful accessing
	 */
	public static Writer writer;

	/**
	 * Count of how many keys are dead when this matches keys.length all keys are blue
	 * and the game is over
	 */
	public int totalDeadKeys = 0;

	/**
	 * List of keys that aren't currently red or blue/dead.
 	 */
	private List<String> unpressed;

	/**
	 * List of all keys able to be used in the game
	 */
	public String[] keys = new String[]{"q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "a", "s", "d", "f", "g", "h", "j", "k", "l", "z", "x", "c", "v", "b", "n", "m",
	                                     "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "f10", "f11", "1", "2", "3", "4", "5", "6",
			"7", "8", "9", "0", "minus", "equal", "lbrace", "colon", "comma", "dot",  };

	/**
	 * Map which contains the current state of each key.
	 */
	private Map<String, KeyState> keyStateMap;

	/**
	 * Parent submarine applet so we can access game over methods
	 */
	private SubmarineApplet parentApplet;

	/**
	 * Initialises values and sets the initial state of the keyboard
	 *
	 * @param parentApplet The Submarine Applet that constructed this class
	 */
	EngineeringButtons(SubmarineApplet parentApplet) {
		this.parentApplet = parentApplet;

		// Initialise unpressed with a list of all keys
		unpressed = new ArrayList<>(Arrays.asList(keys));

		keyStateMap = new HashMap<>();

		// Initialise keyStateMap with a state for each key
		for(String key: keys){
			keyStateMap.put(key, new KeyState(key));
		}

		// Open the file to write commands to the keyboard
		// If this fails it is ignored so be careful when accessing the variable later to make sure its not
		// null first
		try{
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("/dev/input/ckb1/cmd")));
			// Sends command to keyboard separated by a space in the format <command> <argument> <command> <argument>
			// Set the keyboard to its active state
			// set the caps, scroll lock & numpad lights off so people don't get distracted it they press buttons
			// while playing
			// sets all rbg values on the keyboard to black (off).
			writer.write("active ioff caps ioff scroll ioff num rgb 000000\n");
			// After writing make sure to flush so commands are applied immediately.
			writer.flush();
		} catch (IOException ex){
			// all is fine move on
		}

		secsToKey = 5;

		// Set all the keys in the key list to white so you can see what keys your playing on.
		if(writer != null) {
			for (String key : keys) {
				try {
					writer.write(String.format("rgb %s:ffffff ", key));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			try {
				writer.write("\n");
				writer.flush();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		// Add a shutdown hook so the keyboard is put back into its idle state and caps, scroll & num lock
		// are set back to normal.
		Runtime.getRuntime().addShutdownHook(new Thread(this::cleanup));
	}

	public void cleanup(){
		try {
			if(writer != null) {
				writer.write("iauto caps iauto scroll iauto num idle\n");
				writer.flush();
				writer.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void tick(float dt) {
		if(SubmarineApplet.gameOver)
			return;

		if(totalDeadKeys == keys.length){
			System.out.println("You lose");
			parentApplet.endGame(false);
		}

		// Each tick make the time required for the next key to go red exponentially smaller.
		// Should make it harder as the game goes on.
		secsToKeyOff *= Math.pow(RATE_SECS_TO_KEY, dt);

		for(KeyState keyState: keyStateMap.values()){

			// Handle keys currently fading
			if(keyState.timeLeftToFade >=0){
				keyState.timeLeftToFade-=dt;
				// Calculate how far through the fade we are and make sure the colour values don't go
				// below 0.
				int change = Math.max((int) ((255)*((keyState.timeLeftToFade-0)/(GREEN_FADE_TIME)) + 0), 0);
				setKeyColour(keyState.key, String.format("%02xff%02x", 255-change, 255-change));

				// If time to fade gets below zero firmly set it back to -1 so everything knows it
				// is no longer fading.
				if(keyState.timeLeftToFade < 0){
					keyState.timeLeftToFade = -1;
				}
			}

			// Handle keys that are currently red / flashing
			if(keyState.timeToFlash >= 0){
				keyState.timeToFlash-=dt;

				// If key in flashing phase
				if(keyState.timeToFlash <= timeRedFlash){

					int state = (int) (keyState.timeToFlash/FLASH_TIME);

					// If state is even flash light on if odd turn off.
					if(state % 2 == 0){
						setKeyColour(keyState.key, "ff0000");
					} else {
						setKeyColour(keyState.key, "000000");
					}

					// If time timeToFlash is less than 0 user took to long to press so kill
					// the key
					if(keyState.timeToFlash < 0){
						kill(keyState);
					}
				}
			}
		}

		// Handle turning new keys red
		currentTime+=dt;
		if(currentTime > secsToKey && unpressed.size() > 0) {
			makeKeyRed();
			// Reset currentTime and count up to next key
			currentTime = 0;
			// Randomly select how long it will be to the next key
			secsToKey = (Math.random() * (secsToKeyOff + (secsToKeyOff * secsToKeyPercent))) + (secsToKeyOff + (secsToKeyOff * secsToKeyPercent));
		}
	}


	public void makeKeyRed(){
		if(unpressed.size() == 0)
			return;
		// Get random key from unpressed to turn red
		int rnd = (int) (Math.random() * unpressed.size());
		KeyState keyState = keyStateMap.get(unpressed.get(rnd));
		keyState.awaitingPress = true;
		keyState.timeToFlash = timeRed;
		// Make sure to stop any current fade effect that may be happening
		keyState.timeLeftToFade = -1;
		// Set key to red
		setKeyColour(unpressed.get(rnd), "ff0000");
		// Remove key from unpressed so it can't be selected again
		unpressed.remove(rnd);
	}

	/**
	 * Handles when a key is pressed
	 *
	 * if key is currently red mark it pressed
	 * otherwise turn it blue/kill it
	 *
	 * @param keyCode Integer value referencing a key
	 */
	public void keyPressed(int keyCode){
		if(keyStateMap.containsKey(KeyCodes.keyCodes.get(keyCode))){
			KeyState keyState = keyStateMap.get(KeyCodes.keyCodes.get(keyCode));
			if(keyState.awaitingPress){
				// As its now been pressed put it back in the pool of keys that can turn red
				unpressed.add(keyState.key);
				keyState.timeLeftToFade = GREEN_FADE_TIME;
				keyState.awaitingPress = false;
				// Make sure it stops flashing
				keyState.timeToFlash = -1;
				// Set key to green
				setKeyColour(keyState.key, "00ff00");

			} else if(keyState.readyToGoRed()){
				// If key is currently blank when pressed kill it.
				kill(keyState);
			} else if (keyState.dead){
				killRandomKey();
			}
		}
	}

	void killRandomKey(){
		/*if(unpressed.size() == 0)
			return;
		int rnd = (int)(Math.random() * unpressed.size());
		kill(keyStateMap.get(unpressed.get(rnd)));
		*/
		makeKeyRed();
	}

	/**
	 * Sets key in keyState to dead and resets values so the key can;t be used or have its colour changed
	 *
	 * @param keyState state of the key to kill
	 */
	public void kill(KeyState keyState){
		// Remove it from keys that can go red
		unpressed.remove(keyState.key);
		keyState.dead = true;
		// Stop any playing animations
		keyState.timeToFlash = -1;
		keyState.timeLeftToFade = -1;
		keyState.awaitingPress = false;
		// Set colour to blue
		setKeyColour(keyState.key, "0000ff");
		// Increment dead keys so we can tell when all keys are dead
		totalDeadKeys++;

		// Shrink time red so players have less time to press red keys as they
		// kill blue keys.
		timeRed -= MIN_RED_TIME;
		timeRed *= RATE_RED_FASTER;
		timeRed += MIN_RED_TIME;
	}

	/**
	 * Sets the specified keys LED to the specified colour.
	 *
	 * @param key Key to change the LED on
	 * @param colour Colour to change the key to in the format "rrggbb"
	 */
	private void setKeyColour(String key, String colour){
		if(writer != null) {
			try {
				writer.write(String.format("rgb %s:%s\n", key, colour));
				writer.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Stores the current state of each key
	 */
	private class KeyState {
		/**
		 * String name of the key
		 */
		String key;

		/**
		 * If the key is currently waiting for the player to press it
		 */
		boolean awaitingPress = false;

		/**
		 * How long the key has left to fade is seconds
		 * -1 when not playing a fade animation
		 */
		float timeLeftToFade = -1;

		/**
		 * How long a key has left being red. also controls whether it is currently flashed on or off while red
		 * -1 when not red
		 */
		double timeToFlash = -1;

		/**
		 * Whether the key has been set to blue/killed and as such is out of the game
		 */
		boolean dead = false;

		/**
		 * Constructs a state for the provided key
		 *
		 * @param key Key to create a state for
		 */
		KeyState(String key){
			this.key = key;
		}

		/**
		 * @return true if the key is currently able to turn red
		 */
		boolean readyToGoRed() {
			return !awaitingPress && !dead;
		}
	}
}
