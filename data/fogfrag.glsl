#ifdef GL_ES
precision mediump float;
precision mediump int;
#endif

uniform sampler2D texture;

varying vec4 viewSpace;
varying vec2 texcord;

const float FogDensity = 0.05;
const vec3 fogColour = vec3(0, 0, 0.5);

void main() {


  float dist = length(viewSpace);
  float fogFactor = 1.0 /exp(dist * FogDensity);
  fogFactor = clamp( fogFactor, 0.0, 0.5 );

  vec3 tex = texture(texture, texcord).rgb;
  vec3 finalColour = mix(fogColour, tex, fogFactor);

  gl_FragColor = vec4(finalColour, 1);
}